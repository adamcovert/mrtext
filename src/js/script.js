$(document).ready(() => {


  $('.page-header__burger').on('click', function () {
    $('.sidebar').addClass('sidebar--is-active');
  });

  $('.sidebar__close').on('click', function () {
    $('.sidebar').removeClass('sidebar--is-active');
  });



  const featuredArticlesSwiper = new Swiper('.featured-articles__slider', {
    pagination: {
      el: '.featured-articles__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.featured-articles__slider-nav .slider-nav__btn--prev',
      nextEl: '.featured-articles__slider-nav .slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40
      }
    }
  });

  const featuredTestimonialsSwiper = new Swiper('.featured-testimonials__slider', {
    pagination: {
      el: '.featured-testimonials__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.featured-testimonials__slider-nav .slider-nav__btn--prev',
      nextEl: '.featured-testimonials__slider-nav .slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 40
      }
    }
  });
});